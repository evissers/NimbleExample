# Package

version       = "0.1.0"
author        = "Eric Vissers"
description   = "Simple package to learn about Nimble"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["NimbleExample"]


# Dependencies

requires "nim >= 1.4.8", "bigints"
